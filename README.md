# Umbraco v7 Dialog Expander

This package is a simple UI enhancement to the v7 right-side dialog flyours to allow a content editor to make the dialog bigger or back to normal.  It just gives a bit more room for dialog selections and views.  With the new Document Types as grid editors, it can help to have a little extra room when its available.



## Umbraco Install

Download the package from Our Umbraco [https://our.umbraco.org/projects/backoffice-extensions/inline-html-help-label](Inline HTML Help Label) and install into your instance.




## Setup for Development

### Install Dependencies

```bash
cd src
npm install -g grunt-cli
npm install
```

### Build

```bash
grunt
```

### Watch

```bash
grunt watch
```

### Package

```bash
grunt package
```
