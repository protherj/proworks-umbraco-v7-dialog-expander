This package is a simple UI enhancement to the v7 right-side dialog flyours to allow a content editor to make the dialog bigger or back to normal.  It just gives a bit more room for dialog selections and views.  With the new Document Types as grid editors, it can help to have a little extra room when its available.


Git repo: https://bitbucket.org/proworks/proworks-umbraco-v7-dialog-expander

